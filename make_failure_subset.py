#!/usr/bin/env python
import re
from node import *
import read2
import csv
import time

 ###############################################################
 #  Option_name | Possible values   |    dim    |    offset    #
 #      A       |       0,1,2       |    3      |       0      #
 #      B       |-      0,1         |-   2      |       0      #
 #      C       |       1,2,3,4     |    4      |       1      #
 #      D       |-      1,2,3       |-   3      |       1      #
 #      E       |       0,1         |    2      |       0      #
 #      F       |       0,1,2,3     |    4      |       0      #
 #      G       |       1,2,3,4     |    4      |       1      #
 ###############################################################
read = read2.read2(0, "cleandataBEA.csv")

letter = "D"
dim = 3
offset = 1
func = "add(sub(IF1T(D_1, add(O2, O1), IF1T(D_3, O0, IF1T(D_3, coeficientPos(coeficientNeg(O0, 0.8), IF1T(cost_0_605, 0.4, -0.2)), O2))), O2), coeficientPos(coeficientPos(add(O0, O2), IF0T(age_oldest_16_19, 0.9, -1.0)), IF1T(age_oldest_16_19, IF0T(F_1, IF1T(E_0, IF1T(AND(region_GLAK, cost_0_605), IF0T(F_1, 0.5, 0.5), IF1T(NOR(C_1, risk_factor_3), 0.2, 0.1)), 0.5), IF1T(NAND(age_youngest_55P, car_value_f), 0.2, 0.1)), 0.5)))"

index = read.index(str(letter) + ":" + str(offset))
print letter, index, dim

for i in range(dim):
    print read.name(index + i)

if(dim == 2):
    O0 = numpy.array([1,0])
    O1 = numpy.array([0,1])
    Null= numpy.array([0,0])
elif(dim == 3):
    O0 = numpy.array([1,0,0])
    O1 = numpy.array([0,1,0])
    O2 = numpy.array([0,0,1])
    Null = numpy.array([0,0,0])
elif(dim == 4):
    O0 = numpy.array([1,0,0,0])
    O1 = numpy.array([0,1,0,0])
    O2 = numpy.array([0,0,1,0])
    O3 = numpy.array([0,0,0,1])
    Null = numpy.array([0,0,0,0])

TRUE = True 
FALSE = False
#set up variable fom data

failures = []
failuresCount = 0

def addFailure(exp):
    f = []
    for e in exp:
        for i in e[0:-1]:
            f.append(int(i))

        f.append(e[-1])
        failures.append(f)
        f = []

# for name in read.head():
#     index = read.index(name)
#     name = re.sub(":", "_", name)
#     name = re.sub("-", "_", name)
#     name = re.sub("\+", "P", name)
#     name += "_index"
#     print name, index
#     exec("%s=%s" % (name, index))
#calculate result
l = 0
for experiment in read:
    l += 1
    ls = numpy.array([0.0]*dim)
    time.sleep(0.003)
    if(l % 1000 == 0):
        print(l, " of ", 1000)
    #print ""
    #print len(experiment)
    #print ls
    for item in experiment:
        #print item [0]
        #get data 
        for name in read.head():
            ind = read.index(name)
            name = re.sub(":", "_", name)
            name = re.sub("-", "_", name)
            name = re.sub("\+", "P", name)
            exec("%s=%s" % (name, item[ind]))

        if item[0] != 1: #si c'est un magasinage
            exec("ls += " + func)
        else: #un achat
            m = min(ls)
            result = [i for i, j in enumerate(ls) if j == m]
            if(int(item[index + result[0]]) == 1):
                pass
                #print "win", result[0], item[index], item[index+1], item[index+2]
            else:
                #print "loose", result[0], item[index], item[index+1], item[index+2]
                addFailure(experiment)
                failuresCount += 1


def save(name, data, head):
    myfile = open( name + ".csv" , 'wb')
    wr = csv.writer(myfile)
    wr.writerow(head)
    for row in data:
        wr.writerow(row)

head = []
head.append("record_type")
head.extend(["day:sunday", "day:monday", "day:tuesday", "day:wednesday", "day:thursday", "day:friday", "day:saturday"])
head.extend(["time:0-3", "time:3-6", "time:6-9", "time:9-12", "time:12-15", "time:15-18", "time:18-21", "time:21-24"])
head.extend(["region:NENG", "region:MEST", "region:GLAK", "region:PLNS", "region:SEST", "region:SWST", "region:RKMT", "region:FWST"])
head.extend(["group_size:1", "group_size:2", "group_size:3", "group_size:4"])
head.append("homeowner")
head.extend(["car_age:0-5", "car_age:5-10", "car_age:10-15", "car_age:15+"])
head.extend(["car_value:a", "car_value:b", "car_value:c", "car_value:d", "car_value:e", "car_value:f", "car_value:g", "car_value:h", "car_value:i", "car:value:NA"])
head.extend(["risk_factor:1", "risk_factor:2", "risk_factor:3", "risk_factor:4", "risk_factor:NA"])
head.extend(["age_oldest:16-19", "age_oldest:20-24", "age_oldest:25-40", "age_oldest:40-55", "age_oldest:55+"])
head.extend(["age_youngest:16-19", "age_youngest:20-24", "age_youngest:25-40", "age_youngest:40-55", "age_youngest:55+"])
head.append("married_couple")
head.extend(["C_previous:1", "C_previous:2", "C_previous:3", "C_previous:4", "C_previous:NA"])
head.extend(["duration_previous:0-2", "duration_previous:2-5", "duration_previous:5-9", "duration_previous:9+", "duration_previous:NA"])
head.extend(["A:0", "A:1", "A:2"])
head.extend(["B:0", "B:1"])
head.extend(["C:1", "C:2", "C:3", "C:4"])
head.extend(["D:1", "D:2", "D:3"])
head.extend(["E:0", "E:1"])
head.extend(["F:0", "F:1", "F:2", "F:3"])
head.extend(["G:1", "G:2", "G:3", "G:4"])
head.extend(["cost:0-605", "cost:605-635", "cost:635-665", "cost:665+"])
head.append("customer_ID")

print failuresCount
save("failuredata" + letter, failures, head)

#print result, expected value

