#!/usr/bin/python
# this file prepare an output from every categorisation function provided
#####################
# customer_ID,plan  #
# 10000001,1111111  #
# 10000002,1111111  #
# 10000003,1111111  #
# ...               #
#####################


# TODO reorient data
import re
from node import *
import read2

read = read2.read2(110, "cleandata.csv")

#################
# min or max...
optimise = min #or max

functions = {
    "A" : "IF1T(A_1, O0, coeficientNeg(IF0T(IF0T(time_0_3, A_0, state_FL), O2, O0), IF1T(IF0T(time_18_21, state_RI, state_IA), -0.5, 0.5)))",
    "C" : "coeficientNeg(IF1T(C_2, IF0T(state_NH, O1, coeficientPos(coeficientNeg(O0, -0.6), identityf(-1.0))), sub(IF1T(OR(D_1, age_oldest_16_19), O0, coeficientNeg(coeficientNeg(sub(O1, IF1T(OR(D_1, age_oldest_16_19), coeficientPos(IF1T(IF1T(time_3_6, state_IA, time_9_12), coeficientNeg(O0, identityf(-0.2)), IF0T(car_age_10_15, coeficientPos(sub(Null, O1), -0.3), O0)), identityf(0.7)), coeficientNeg(O1, 0.9))), identityf(-0.3)), 0.9)), IF0T(NOR(C_previous_1, D_1), IF0T(F_2, Null, Null), IF1T(state_IA, O0, O0)))), identityf(-0.2))",
    "D" : "IF1T(NAND(D_2, IF0T(NAND(OR(state_CO, D_2), IF0T(car_value_a, NOR(state_WI, D_3), state_IA)), NOR(state_WI, D_3), state_IA)), IF0T(NOR(state_WI, D_3), add(O2, O2), O0), coeficientNeg(sub(sub(O0, O1), O1), IF1T(state_NY, IF1T(state_NY, 0.3, 0.7), 0.7)))",
    "E" : "coeficientPos(IF1T(NAND(IF0T(state_IA, record_type, state_RI), AND(AND(IF0T(state_NY, day_sunday, car_age_5_10), NAND(age_oldest_16_19, car_value_c)), OR(IF1T(C_4, state_KY, OR(state_WV, homeowner)), NOT(state_NM)))), coeficientPos(coeficientNeg(coeficientNeg(IF0T(E_0, O1, O0), -0.6), IF1T(NOT(B_0), IF0T(day_sunday, -0.8, -0.9), IF0T(state_WV, -0.7, -0.2))), IF0T(IF0T(OR(time_18_21, age_youngest_20_24), car_value_g, IF1T(time_3_6, state_WV, G_1)), IF1T(XOR(age_youngest_16_19, state_IN), IF1T(state_WV, -1.0, IF0T(day_friday, 0.1, -0.1)), IF1T(time_9_12, 0.4, 0.6)), 0.4)), sub(add(IF0T(XOR(A_2, state_GA), O0, sub(Null, Null)), IF1T(IF0T(state_IA, record_type, state_SD), coeficientNeg(O0, 0.5), coeficientNeg(sub(O0, O1), IF1T(IF0T(G_4, day_sunday, F_0), IF0T(state_RI, -0.5, -0.6), IF1T(XOR(day_sunday, group_size_4), -1.0, -0.8))))), sub(IF1T(IF1T(state_MS, IF1T(time_3_6, state_WV, G_1), age_youngest_25_40), sub(O0, O1), add(Null, O0)), IF1T(NAND(AND(car_age_15P, NOT(AND(G_1, state_SD))), AND(AND(IF0T(state_NY, day_sunday, car_age_5_10), NAND(G_1, car_value_c)), OR(E_1, NOT(state_NM)))), coeficientPos(coeficientNeg(coeficientNeg(IF0T(E_0, O1, coeficientPos(Null, 0.0)), IF1T(state_MT, -0.3, -0.5)), IF1T(NOT(B_0), IF0T(day_sunday, -0.8, -0.9), IF0T(D_3, -0.7, -0.2))), IF0T(IF0T(OR(time_18_21, age_youngest_20_24), NOR(state_OK, E_1), IF1T(time_3_6, state_WV, G_1)), IF1T(XOR(age_youngest_16_19, state_IN), IF1T(state_WV, -1.0, IF0T(day_friday, 0.1, -0.1)), IF1T(time_9_12, 0.4, 0.6)), IF0T(IF1T(group_size_3, state_CT, D_2), IF0T(car_value_e, 0.2, 0.7), IF0T(time_15_18, -0.8, 0.5)))), sub(add(IF0T(XOR(A_2, state_GA), O0, sub(Null, Null)), O0), sub(IF1T(IF1T(state_MS, car_value_g, age_youngest_25_40), sub(O0, O1), coeficientPos(Null, 0.0)), add(IF1T(car_value_c, coeficientNeg(Null, -0.9), O0), add(Null, O1)))))))), -0.8)",
    "F" : "coeficientPos(O2, IF0T(NOT(NAND(state_SD, D_2)), IF0T(F_2, 0.7, -0.9), IF1T(state_ID, IF1T(IF0T(NOR(state_KS, state_RI), IF0T(time_18_21, state_RI, state_AR), IF1T(C_4, state_RI, OR(car_value_e, state_MO))), IF1T(NAND(B_1, C_2), -0.7, IF0T(A_2, -0.2, 0.6)), IF1T(NAND(C_4, age_youngest_16_19), 0.5, IF0T(state_ID, 0.6, 0.2))), 0.2)))",
    "G" : "coeficientPos(coeficientPos(IF1T(car_value_i, O2, add(add(sub(O1, IF0T(IF1T(state_PA, state_DE, G_3), coeficientPos(Null, 0.2), IF0T(A_0, O2, O2))), IF1T(time_9_12, Null, IF1T(time_9_12, Null, O3))), coeficientPos(IF1T(state_CO, O0, Null), IF0T(car_value_b, -0.8, -0.6)))), IF0T(car_value_f, -0.8, -0.6)), IF0T(NOT(G_2), 0.2, -0.3))"
}
predictionParameters = {
    "A" : { "length" : 3, "offset" : 0 },
    "B" : { "length" : 2, "offset" : 0 },
    "C" : { "length" : 4, "offset" : 1 },
    "D" : { "length" : 3, "offset" : 1 },
    "E" : { "length" : 2, "offset" : 0 },
    "F" : { "length" : 4, "offset" : 0 },
    "G" : { "length" : 4, "offset" : 1 }
}

results = []

TRUE = True 
FALSE = False
# init
prediction = ""
for experiment in read:

    prediction = ""
    for param in functions:
        function = functions[param]
        length = predictionParameters[param]["length"]
        offset = predictionParameters[param]["offset"]
        ls = numpy.array([0.0]*length)
        if(length == 2):
            O0 = numpy.array([1,0])
            O1 = numpy.array([0,1])
            Null= numpy.array([0,0])
        elif(length == 3):
            O0 = numpy.array([1,0,0])
            O1 = numpy.array([0,1,0])
            O2 = numpy.array([0,0,1])    
            Null = numpy.array([0,0,0])
        elif(length == 4):
            O0 = numpy.array([1,0,0,0])
            O1 = numpy.array([0,1,0,0])
            O2 = numpy.array([0,0,1,0])
            O3 = numpy.array([0,0,0,1])
            Null = numpy.array([0,0,0,0])

            
        #print param, length, offset
        #print function
        if(function != ""):
            index = read.index(str(param) + ":" + str(offset))

            for item in experiment:
                customer_id = str(item[-1])
                #get data from the train
                for name in read.head():
                    ind = read.index(name)
                    name = re.sub(":", "_", name)
                    name = re.sub("-", "_", name)
                    name = re.sub("\+", "P", name)
                    exec("%s=%s" % (name, item[ind]))

                if item[0] != 1: #si c'est un magasinage
                    exec("ls += " + function)

                else:

                    m = optimise(ls)
                    result = [i for i, j in enumerate(ls) if j == m]
                    prediction = prediction + str(result[0] + offset)
    results.append(customer_id + "," + prediction)

#print result to file
print results