-p G
Reading...
Ready!, ( 97008 purchases )
position: 115 116 117 118 
Ready!, ( 97008 purchases )
position: 115 116 117 118 
predictionParameter G
predictionParameterLength 4
nExps 5000
nRead 0
nIndiv 200
nHof 20
nGen 30
genMinTreeHeight 1
genMaxTreeHeight 11
   	      	                       fitness                        	              size             
   	      	------------------------------------------------------	-------------------------------
gen	nevals	avg     	max     	min     	std   	avg    	max  	min	std    
0  	200   	-1206.41	-258.898	-2054.82	402.58	1668.59	24510	3  	4555.44
1  	118   	-1482.02	-423.972	-2070.77	383.023	569.515	12066	1  	1727.41
2  	103   	-1684.8 	-947.852	-2041.99	337.081	155.53 	8033 	1  	735.079
3  	114   	-1714.49	-448.982	-2041.99	388.013	30.45  	3265 	1  	237.45 
4  	124   	-1759.44	-410.941	-2041.99	377.162	31.175 	3259 	1  	236.776
5  	107   	-1816.71	-408.981	-2041.99	328.418	11.095 	285  	1  	21.1101
6  	122   	-1774.19	-869.786	-2464.77	366.165	9.54   	37   	1  	7.4571 
7  	105   	-1826.3 	-267.965	-2464.77	349.323	10.115 	53   	1  	8.09517
8  	109   	-1848.32	-407.947	-2464.77	351.186	11.495 	46   	1  	8.87975
9  	132   	-1834.86	-485.929	-2495.76	375.395	11.89  	48   	1  	10.6545
10 	131   	-1884.71	-948.879	-2583.13	394.666	13.68  	49   	1  	11.9832
11 	116   	-1890.41	-421.959	-2746.14	514.929	18.335 	57   	1  	15.6746
12 	115   	-2022.2 	-431.973	-2972.14	539.021	26.52  	65   	1  	18.4149
13 	115   	-2076.64	-427.791	-3256.41	623.655	32.25  	87   	1  	19.3806
14 	103   	-2283.14	-445.782	-3256.41	529.883	38.395 	96   	1  	19.4007
15 	117   	-2198.07	-392.928	-3326.16	720.542	40.705 	110  	1  	19.3147
16 	125   	-2438.38	-426.97 	-3335.68	627.824	47.14  	110  	6  	17.133 
17 	106   	-2628.68	-453.501	-3335.68	663.575	52.045 	118  	7  	17.6245
18 	117   	-2677.43	-364.961	-3335.68	815.863	55.36  	108  	3  	16.8104
19 	109   	-2879.72	-868.482	-3339.09	635.594	59.815 	128  	7  	17.1129
20 	120   	-2978.29	-453.918	-3374.02	599.417	64.715 	128  	1  	18.0365
21 	109   	-3001.87	-449.804	-3421.69	585.387	66.94  	137  	7  	18.1476
22 	118   	-2974.14	-450.901	-3406.85	675.959	69.2   	128  	9  	18.8801
23 	113   	-2931.44	-454.58 	-3455.8 	689.098	68.38  	168  	3  	19.5636
24 	115   	-2999.73	-388.511	-3467.15	695.163	68.565 	122  	7  	16.7769
25 	112   	-2982.1 	-459.618	-3467.15	712.839	68.515 	137  	1  	20.4727
26 	110   	-3004.04	-369.365	-3537.13	715.533	70.035 	186  	7  	20.784 
27 	103   	-3143.75	-436.356	-3689.37	537.894	69.11  	107  	25 	12.7549
28 	99    	-3135.92	-422.723	-3689.37	604.873	68.83  	136  	7  	17.9073
29 	106   	-3081.68	-667.79 	-3786.48	690.503	71.82  	135  	12 	18.234 
30 	116   	-3149.12	-296.632	-3754.32	656.936	72.38  	125  	7  	17.329 
0 (-3786.484103999831,) coeficientPos(coeficientPos(IF1T(car_value_i, O2, add(add(sub(O1, IF0T(IF1T(state_PA, state_DE, G_3), coeficientPos(Null, 0.2), IF0T(A_0, O2, O2))), IF1T(time_9_12, Null, IF1T(time_9_12, Null, O3))), coeficientPos(IF1T(state_CO, O0, Null), IF0T(car_value_b, -0.8, -0.6)))), IF0T(car_value_f, -0.8, -0.6)), IF0T(NOT(G_2), 0.2, -0.3))
1 (-3765.849115999763,) coeficientPos(coeficientPos(IF1T(car_value_i, O2, add(add(sub(O1, IF0T(IF1T(state_PA, state_DE, G_3), coeficientPos(Null, 0.2), IF0T(A_0, O2, coeficientPos(coeficientPos(O0, 0.6), IF1T(state_MT, -0.1, -1.0))))), IF1T(time_9_12, Null, IF1T(time_9_12, Null, O3))), coeficientPos(IF1T(state_CO, O0, Null), IF0T(XOR(duration_previous_NA, state_NY), -0.8, 0.7)))), IF0T(car_value_f, -0.8, -0.6)), IF0T(NOT(G_2), 0.2, -0.3))
2 (-3754.3175020003328,) coeficientPos(coeficientPos(IF1T(car_value_i, O2, add(add(sub(O1, IF0T(IF1T(state_PA, state_DE, G_3), coeficientPos(Null, 0.2), IF0T(A_0, O2, O2))), IF1T(time_9_12, Null, IF1T(time_9_12, Null, O3))), coeficientPos(IF1T(state_CO, O0, Null), IF0T(car_value_b, -0.8, -0.6)))), IF0T(car_value_f, -0.8, IF0T(car_value_f, -0.8, -0.6))), IF0T(NOT(G_2), 0.2, -0.3))
3 (-3726.695220000268,) coeficientPos(coeficientPos(IF1T(car_value_i, O2, add(add(sub(O1, IF0T(IF1T(state_PA, state_DE, G_3), coeficientPos(Null, 0.2), IF0T(A_0, O2, coeficientPos(coeficientPos(O0, 0.6), IF1T(state_MT, -0.1, -1.0))))), IF1T(time_9_12, Null, IF1T(time_9_12, Null, O3))), coeficientPos(IF1T(car_value_i, O0, Null), IF0T(XOR(duration_previous_NA, state_NY), -0.8, -0.6)))), IF0T(car_value_f, -0.8, -0.6)), IF0T(NOT(G_2), 0.2, IF0T(NOT(G_2), 0.2, -0.3)))
4 (-3722.340350000326,) coeficientPos(coeficientPos(IF1T(car_value_i, O2, add(add(sub(O1, IF0T(IF1T(state_PA, state_DE, G_3), coeficientPos(Null, 0.2), IF0T(A_0, O2, O2))), IF1T(time_9_12, Null, IF1T(time_9_12, Null, O3))), coeficientPos(IF1T(state_CO, O0, Null), IF0T(car_value_b, -0.8, IF0T(car_value_f, -0.8, -0.6))))), IF0T(car_value_f, -0.8, -0.6)), IF0T(NOT(G_2), 0.2, -0.3))
5 (-3718.3729740002673,) coeficientPos(coeficientPos(IF1T(car_value_i, O2, add(add(sub(O1, IF0T(IF1T(state_PA, state_DE, G_3), coeficientPos(Null, 0.2), IF0T(A_0, O2, O2))), IF1T(time_9_12, Null, IF1T(time_9_12, Null, O3))), coeficientPos(IF1T(state_CO, O0, Null), IF0T(car_value_b, IF1T(NOT(G_2), -0.4, 0.1), -0.6)))), IF0T(car_value_f, -0.8, -0.6)), IF0T(NOT(G_2), 0.2, -0.3))
6 (-3710.7316799998357,) coeficientPos(coeficientPos(IF1T(car_value_i, O2, add(add(sub(O1, IF0T(IF1T(state_PA, state_DE, G_3), coeficientPos(Null, 0.2), IF0T(A_0, O2, coeficientPos(coeficientPos(O0, 0.6), IF1T(state_MT, -0.1, -1.0))))), IF1T(time_9_12, Null, IF1T(time_9_12, Null, O3))), coeficientPos(IF1T(state_CO, O0, Null), IF0T(XOR(duration_previous_NA, state_NY), -0.8, -0.6)))), IF0T(car_value_f, -0.8, -0.6)), IF0T(NOT(G_2), 0.2, IF0T(NOT(G_2), 0.2, -0.3)))
7 (-3696.906799999774,) coeficientPos(coeficientPos(IF1T(car_value_i, O2, add(add(sub(O1, IF0T(IF1T(state_PA, state_DE, G_3), coeficientPos(Null, 0.2), IF0T(A_0, O2, coeficientPos(coeficientPos(O0, 0.6), IF1T(state_MT, -0.1, -1.0))))), IF1T(time_9_12, Null, IF1T(time_9_12, Null, O3))), coeficientPos(IF1T(state_CO, O0, Null), IF0T(XOR(duration_previous_NA, state_NY), -0.8, 0.7)))), IF0T(car_value_f, -0.6, -0.6)), IF0T(NOT(G_2), 0.2, -0.3))
8 (-3689.371295999928,) coeficientPos(coeficientPos(IF1T(car_value_i, O2, add(add(sub(O1, IF0T(IF1T(state_PA, state_DE, G_3), coeficientPos(Null, 0.2), IF0T(A_0, O2, O2))), IF1T(time_9_12, Null, IF1T(time_9_12, Null, O3))), coeficientPos(IF1T(state_CO, O0, Null), IF0T(XOR(duration_previous_NA, state_NY), -0.8, -0.6)))), IF0T(car_value_f, -0.8, -0.6)), IF0T(NOT(G_2), 0.2, -0.3))
9 (-3686.47236000016,) coeficientPos(coeficientPos(IF1T(G_1, O2, add(add(sub(O1, IF0T(IF1T(state_OK, state_DE, IF0T(XOR(time_15_18, age_youngest_20_24), IF1T(G_1, car_age_15P, risk_factor_NA), OR(F_1, B_1))), coeficientPos(sub(sub(O2, coeficientNeg(Null, 0.1)), coeficientPos(O2, 0.1)), IF1T(time_3_6, -0.6, 0.2)), IF0T(A_0, O2, O2))), IF1T(age_youngest_20_24, Null, O3)), coeficientPos(IF1T(state_CO, O0, Null), IF1T(NOT(G_2), -0.4, 0.1)))), IF0T(car_value_f, -0.8, -0.6)), IF0T(NOT(G_2), 0.2, -0.3))
10 (-3671.920350000166,) coeficientPos(coeficientPos(IF1T(car_value_i, O2, add(add(sub(O1, IF0T(IF1T(state_PA, state_DE, G_3), coeficientPos(Null, 0.2), IF0T(A_0, O2, coeficientPos(coeficientPos(O0, 0.6), IF1T(state_MT, -0.1, -1.0))))), IF1T(time_9_12, Null, IF1T(time_9_12, Null, O3))), coeficientPos(IF1T(state_CO, O0, Null), IF0T(XOR(duration_previous_NA, state_NY), -0.8, -0.6)))), IF0T(car_value_f, -0.8, -0.6)), IF0T(NOT(G_2), 0.2, -0.3))
11 (-3654.5614800002495,) coeficientPos(coeficientPos(IF1T(car_value_i, O2, add(add(sub(O1, IF0T(IF1T(state_PA, state_DE, G_3), coeficientPos(Null, 0.2), IF0T(A_0, O2, coeficientPos(coeficientPos(O0, 0.6), IF1T(state_MT, -0.1, -1.0))))), IF1T(NAND(IF0T(age_youngest_16_19, F_3, state_WI), AND(A_1, G_3)), Null, IF1T(time_9_12, Null, O3))), coeficientPos(IF1T(state_CO, O0, Null), IF0T(XOR(duration_previous_NA, state_NY), -0.8, -0.6)))), IF0T(car_value_f, -0.8, -0.6)), IF0T(NOT(G_2), 0.2, -0.3))
12 (-3653.5331640000877,) coeficientPos(coeficientPos(IF1T(G_1, O2, add(add(sub(O1, IF0T(IF1T(state_OK, state_DE, IF0T(XOR(time_15_18, age_youngest_20_24), IF1T(G_1, car_age_15P, risk_factor_NA), OR(F_1, B_1))), coeficientPos(sub(sub(O2, O2), coeficientPos(O2, 0.1)), IF1T(time_3_6, -0.6, 0.2)), IF0T(A_0, O2, O2))), IF1T(age_youngest_20_24, Null, O3)), coeficientPos(IF1T(state_CO, O0, coeficientPos(O2, -0.6)), IF1T(NOT(G_2), 0.2, 0.1)))), IF0T(car_value_f, -0.8, -0.6)), IF0T(NOT(G_2), 0.2, -0.3))
13 (-3640.406183999929,) coeficientPos(coeficientPos(IF1T(car_value_i, O2, add(add(sub(O1, IF0T(IF1T(state_PA, state_DE, G_3), coeficientPos(Null, 0.2), IF0T(A_0, O2, O2))), IF1T(time_9_12, Null, IF1T(time_9_12, Null, O3))), coeficientPos(IF1T(IF1T(record_type, F_1, time_6_9), O0, Null), IF0T(car_value_b, -0.8, -0.6)))), IF0T(car_value_f, -0.8, -0.6)), IF0T(NOT(G_2), 0.2, -0.3))
14 (-3577.231370000256,) coeficientPos(coeficientPos(IF1T(G_1, O2, add(add(sub(O1, IF0T(IF1T(car_value_i, state_DE, IF0T(time_3_6, IF1T(G_1, risk_factor_1, risk_factor_NA), OR(group_size_3, B_1))), coeficientPos(IF1T(state_CO, O0, sub(O1, IF0T(IF1T(risk_factor_4, state_DE, IF0T(XOR(time_15_18, age_youngest_20_24), IF1T(G_1, car_age_15P, risk_factor_NA), OR(F_1, B_1))), IF1T(AND(age_youngest_40_55, risk_factor_NA), IF0T(C_previous_1, O0, Null), coeficientNeg(O0, -0.9)), IF0T(A_0, O2, O2)))), IF1T(NOR(age_youngest_55P, IF1T(NAND(homeowner, group_size_1), IF1T(time_0_3, risk_factor_2, homeowner), IF0T(D_2, G_1, state_MT))), -0.5, IF0T(car_value_f, -0.8, -0.6))), O2)), IF1T(time_9_12, Null, O3)), coeficientPos(IF1T(state_CO, O0, Null), IF1T(NOT(G_2), -0.4, 0.1)))), IF0T(car_value_f, -0.8, -0.6)), IF0T(NOT(G_2), 0.2, -0.3))
15 (-3566.0163159997956,) coeficientPos(coeficientPos(IF1T(car_value_i, O2, add(add(sub(O1, IF0T(IF1T(state_PA, state_DE, G_3), coeficientPos(Null, 0.2), IF0T(A_0, O2, coeficientPos(coeficientPos(O2, 0.6), IF1T(state_MT, -0.1, -1.0))))), IF1T(time_9_12, Null, IF1T(time_9_12, Null, O3))), coeficientPos(IF1T(state_CO, O0, Null), IF0T(XOR(duration_previous_NA, state_NY), -0.8, 0.7)))), IF0T(car_value_f, -0.8, -0.6)), IF0T(NOT(G_2), 0.2, -0.3))
16 (-3549.2622439998354,) coeficientPos(coeficientPos(IF1T(G_1, O2, add(add(sub(O1, IF0T(IF1T(car_value_i, state_DE, IF0T(time_3_6, IF1T(G_1, risk_factor_1, risk_factor_NA), OR(group_size_3, B_1))), coeficientPos(IF1T(state_CO, O0, Null), IF1T(NOR(age_youngest_55P, IF1T(NAND(homeowner, group_size_1), IF1T(time_0_3, risk_factor_2, homeowner), IF0T(D_2, G_1, state_MT))), -0.5, IF0T(car_value_f, -0.8, -0.6))), O2)), IF1T(time_9_12, Null, O3)), coeficientPos(IF1T(state_CO, O0, Null), -0.3))), IF0T(car_value_f, -0.8, -0.6)), IF0T(NOT(G_2), 0.2, -0.3))
17 (-3540.9728800001385,) coeficientPos(coeficientPos(IF1T(G_1, O2, add(add(sub(O1, IF0T(IF1T(car_value_i, state_DE, IF0T(time_3_6, IF1T(G_1, risk_factor_1, risk_factor_NA), OR(group_size_3, B_1))), coeficientPos(IF1T(state_CO, O1, Null), IF1T(NOR(age_youngest_55P, IF1T(NAND(homeowner, group_size_1), IF1T(time_0_3, risk_factor_2, homeowner), IF0T(D_2, G_1, state_MT))), -0.5, IF0T(car_value_f, -0.8, 0.2))), O2)), IF1T(time_9_12, Null, O3)), coeficientPos(IF1T(state_CO, O0, Null), IF1T(state_IN, -0.4, 0.1)))), IF0T(car_value_f, -0.8, -0.6)), IF0T(NOT(G_2), IF0T(NOT(G_2), 0.2, -0.3), -0.3))
18 (-3537.1332280000793,) coeficientPos(coeficientPos(IF1T(G_1, O2, add(add(sub(O1, IF0T(IF1T(car_value_i, state_DE, IF0T(time_3_6, IF1T(G_1, risk_factor_1, risk_factor_NA), OR(group_size_3, B_1))), coeficientPos(IF1T(state_CO, O0, Null), IF1T(NOR(age_youngest_55P, IF1T(NAND(homeowner, group_size_1), IF1T(time_0_3, risk_factor_2, homeowner), IF0T(D_2, G_1, state_MT))), -0.5, IF0T(car_value_f, -0.8, 0.2))), O2)), IF1T(time_9_12, Null, O3)), coeficientPos(IF1T(state_CO, O0, Null), IF1T(NOT(G_2), -0.4, 0.1)))), IF0T(car_value_f, -0.8, -0.6)), IF0T(NOT(G_2), 0.2, -0.3))
19 (-3537.1261459997577,) coeficientPos(coeficientPos(IF1T(G_1, O2, add(add(sub(O1, IF0T(IF1T(car_value_i, state_DE, IF0T(time_3_6, IF1T(G_1, risk_factor_1, risk_factor_NA), OR(group_size_3, B_1))), coeficientPos(IF1T(state_CO, O0, Null), IF1T(NOR(age_youngest_55P, IF1T(NAND(homeowner, group_size_1), IF1T(time_0_3, risk_factor_2, homeowner), IF0T(D_2, G_1, state_MT))), -0.5, IF0T(car_value_f, -0.8, -0.6))), O2)), IF1T(time_9_12, Null, O3)), coeficientPos(IF1T(state_CO, O0, Null), IF1T(NOT(G_2), -0.4, 0.1)))), IF0T(car_value_f, -0.8, -0.6)), IF0T(NOT(G_2), 0.2, -0.3))

position: 115 116 117 118 
-p G
-n 7000
-i 300
Reading...
Ready!, ( 97008 purchases )
position: 115 116 117 118 
predictionParameter G
predictionParameterLength 4
nExps 7000
nRead 0
nIndiv 300
nHof 20
nGen 30
genMinTreeHeight 1
genMaxTreeHeight 11
   	      	                        fitness                        	              size             
   	      	-------------------------------------------------------	-------------------------------
gen	nevals	avg     	max     	min     	std    	avg    	max  	min	std    
0  	300   	-1683.42	-566.916	-3167.77	533.726	1677.89	25976	3  	4840.43
1  	170   	-1992.56	-590.987	-3431.88	502.779	633.233	16114	1  	1939.66
2  	167   	-2236.79	-619.854	-3402.88	501.299	240.43 	16114	1  	1214.61
3  	160   	-2439.9 	-575.974	-3402.88	472.881	87.7667	4787 	1  	410.814
4  	182   	-2411.8 	-591.963	-3402.88	557.66 	31.7233	1116 	1  	134.207
5  	167   	-2492.87	-602.924	-4549.76	554.891	26.67  	1099 	1  	97.2624
6  	193   	-2559.94	-583.886	-4549.76	557.082	29.7967	497  	1  	53.2947
7  	164   	-2698.49	-1215.45	-4549.76	519.799	40.0467	156  	1  	54.3711
8  	166   	-2785.38	-889.881	-4523.57	561.809	68.6467	160  	1  	62.8258
9  	164   	-2967.12	-978.436	-4523.57	484.49 	85.8   	162  	3  	60.9178
10 	158   	-3049.87	-395.917	-4566.94	704.823	70.3867	179  	1  	61.1347
11 	174   	-3206.11	-305.976	-4808.2 	815.637	42.1033	170  	1  	49.4693
12 	165   	-3389.3 	-597.907	-4566.65	924.059	27.44  	158  	1  	31.9413
13 	171   	-3753.62	-997.896	-4587.54	858.953	21.4433	154  	1  	21.5004
14 	173   	-3832.87	-355.806	-4576.61	994.356	19.0133	73   	1  	13.8951
15 	170   	-3967   	-1468.97	-4582.25	842.853	18.8533	73   	1  	12.6593
16 	173   	-3993.47	-763.824	-4604.81	901.526	19.7567	96   	1  	11.5789
17 	178   	-4052.3 	-759.76 	-4604.81	864.048	21.2633	73   	1  	10.6631
18 	179   	-4025.16	-628.946	-4604.81	872.777	20.82  	60   	1  	10.4582
19 	186   	-3996.6 	-407.95 	-4604.81	901.295	21.1833	63   	3  	9.31753
20 	174   	-4107.11	-616.724	-4604.81	808.487	22.0067	50   	1  	9.15059
21 	162   	-4173.24	-1156.86	-4609.6 	776.062	22.5767	45   	1  	8.5719 
22 	169   	-4085.28	-483.921	-4609.6 	817.4  	22.79  	51   	1  	8.91736
23 	177   	-4162.31	-497.898	-4609.6 	807.217	22.8067	51   	1  	9.27592
24 	177   	-4209.5 	-1148.54	-4609.6 	727.833	22.94  	49   	1  	8.53638
25 	191   	-4171.32	-1407.78	-4622.41	748.768	22.2733	49   	1  	7.95814
26 	159   	-4176.95	-452.755	-4628.61	797.035	22.1367	49   	1  	8.19866
27 	161   	-4119.83	-518.894	-4622.41	834.273	23.11  	49   	1  	8.22463
28 	166   	-4193.69	-476.83 	-4622.41	788.296	23.68  	53   	1  	8.91614
29 	180   	-4236.69	-471.8  	-5214.42	694.753	24.4133	57   	3  	9.01494
30 	177   	-4194.06	-485.92 	-4622.41	758.779	24.6267	56   	1  	9.60003
0 (-5214.424767999816,) IF1T(G_3, sub(sub(Null, Null), add(O3, O2)), coeficientNeg(add(O1, coeficientPos(coeficientNeg(O1, -0.6), IF1T(G_3, 0.7, 0.5))), IF0T(G_1, 0.2, -1.0)))
1 (-4808.199998000086,) coeficientPos(IF0T(time_21_24, IF0T(G_3, O0, sub(add(O1, O1), IF0T(A_2, O2, Null))), O1), IF0T(G_1, 0.6, IF1T(IF1T(state_OR, C_4, age_youngest_25_40), IF0T(OR(time_0_3, time_12_15), IF1T(state_MT, -0.2, -0.6), IF1T(age_oldest_55P, 0.5, IF1T(E_0, -0.4, -1.0))), IF0T(day_sunday, 0.1, -0.2))))
2 (-4628.610999999911,) IF1T(G_3, sub(sub(O0, Null), add(O3, O2)), coeficientNeg(add(O1, coeficientPos(coeficientNeg(Null, -0.6), IF1T(G_3, 0.7, 0.5))), IF1T(G_3, 0.9, 0.2)))
3 (-4622.409343999834,) IF1T(G_3, sub(sub(Null, Null), add(coeficientNeg(add(O1, coeficientPos(O0, 0.2)), IF1T(OR(time_0_3, state_TN), IF1T(day_friday, 0.3, -0.2), IF1T(G_3, 0.7, 0.5))), O2)), O0)
4 (-4609.890838000391,) IF1T(G_3, sub(sub(Null, Null), add(O3, O2)), coeficientNeg(add(O1, coeficientPos(coeficientNeg(Null, -0.6), IF1T(G_3, 0.7, IF1T(OR(C_2, married_couple), IF0T(duration_previous_5_9, -0.5, 0.2), IF0T(IF1T(IF1T(age_oldest_40_55, G_1, time_21_24), NOT(car_age_10_15), NOT(state_WA)), IF0T(state_PA, 0.0, 0.1), 0.8))))), IF1T(G_3, 0.9, 0.2)))
5 (-4609.598255999702,) IF1T(G_3, sub(sub(Null, Null), add(O3, O2)), coeficientNeg(add(O1, coeficientPos(coeficientNeg(Null, -0.6), IF1T(G_3, 0.7, 0.5))), IF1T(G_3, 0.9, 0.2)))
6 (-4604.81165199988,) IF1T(G_3, sub(sub(Null, Null), add(O3, O2)), coeficientNeg(add(O1, coeficientPos(O2, IF1T(G_3, 0.7, 0.5))), IF1T(G_3, 0.9, 0.2)))
7 (-4603.835180000201,) IF1T(G_3, sub(sub(Null, Null), add(O3, O2)), coeficientNeg(add(O1, coeficientPos(coeficientNeg(Null, -0.6), IF1T(G_3, IF1T(OR(state_CO, risk_factor_NA), IF0T(cost_605_635, 0.4, 0.7), IF1T(C_1, 0.1, -1.0)), 0.5))), IF1T(G_3, 0.9, 0.2)))
8 (-4603.600079999703,) IF1T(G_3, sub(sub(Null, Null), add(O3, O2)), coeficientNeg(add(O1, coeficientPos(coeficientNeg(Null, -0.6), IF1T(G_3, 0.7, 0.5))), IF1T(G_3, 0.2, 0.2)))
9 (-4601.821632000209,) IF1T(G_3, sub(sub(Null, Null), add(coeficientNeg(sub(sub(Null, Null), IF0T(state_IN, O1, O3)), IF1T(A_2, -0.5, -1.0)), O2)), O0)
10 (-4599.435660000023,) IF1T(G_3, sub(coeficientNeg(coeficientPos(O3, 0.9), IF1T(G_3, -1.0, 0.2)), add(coeficientNeg(add(O1, O3), IF1T(state_MT, IF1T(married_couple, 0.8, -0.4), 0.2)), O2)), O0)
11 (-4597.436340000022,) IF1T(G_3, sub(sub(Null, Null), add(O3, O2)), coeficientNeg(add(O1, coeficientPos(coeficientNeg(Null, -0.6), IF1T(G_3, 0.7, 0.5))), IF1T(duration_previous_0_2, 0.9, 0.2)))
12 (-4596.479748000083,) IF1T(G_3, sub(sub(sub(Null, Null), add(O3, O2)), add(coeficientNeg(add(O1, coeficientPos(O0, 0.2)), IF1T(OR(time_0_3, state_TN), IF1T(day_friday, 0.3, -0.2), IF1T(G_3, 0.7, IF0T(NOR(risk_factor_2, A_2), IF1T(time_21_24, 0.0, 0.6), IF1T(car_value_b, -0.5, 0.9))))), O2)), O0)
13 (-4595.563060000239,) IF1T(G_3, sub(sub(Null, Null), add(O3, O2)), coeficientNeg(add(O1, coeficientPos(coeficientNeg(Null, -0.6), IF1T(G_3, 0.7, IF1T(OR(C_2, married_couple), IF0T(duration_previous_5_9, -0.5, 0.6), IF0T(state_MS, IF0T(state_PA, 0.0, 0.1), 0.8))))), IF1T(G_3, 0.9, 0.2)))
14 (-4594.464936000137,) IF1T(G_3, sub(sub(Null, Null), add(coeficientNeg(add(sub(sub(Null, Null), add(O3, O2)), O3), IF1T(state_MT, IF1T(state_NV, -0.9, IF1T(G_3, 0.9, 0.2)), 0.2)), O2)), O0)
15 (-4594.225943999858,) IF1T(G_3, sub(coeficientPos(O1, 0.4), add(O3, O2)), coeficientNeg(add(O1, coeficientPos(coeficientNeg(Null, -0.6), IF1T(G_3, 0.7, IF0T(day_saturday, 0.4, -1.0)))), IF1T(car_value_h, 0.9, 0.2)))
16 (-4594.145329999484,) IF1T(G_3, sub(sub(Null, Null), add(O3, O2)), coeficientNeg(add(O1, O1), IF1T(G_3, 0.9, 0.2)))
17 (-4592.742792000293,) add(IF0T(G_3, Null, O1), O0)
18 (-4591.282217999558,) IF1T(G_3, sub(sub(Null, Null), add(O3, O2)), coeficientNeg(add(O1, coeficientPos(coeficientNeg(Null, -0.6), 0.3)), IF1T(NOR(NOT(car_value_NA), NAND(married_couple, state_CT)), 0.9, 0.2)))
19 (-4590.695871999785,) IF1T(G_3, sub(coeficientNeg(coeficientPos(O3, 0.9), IF1T(G_3, -1.0, 0.2)), add(coeficientNeg(add(O1, O3), IF1T(state_MT, 0.2, 0.2)), O2)), O0)
