#!/usr/bin/python
#this file provide seed valeur depending on class needed

class seed(object):

    def __init__(self):
        self.individuals = {
            "A" : 
            { 90.01 : "IF0Ta(A_1, add(IF0Ta(IF1Tb(IF1Tb(IF1Tb(group_size_2, car_value_b, car_value_b), XOR(car_age_15P, time_12_15), F_1), OR(E_0, region_SEST), IF1Tb(A_1, E_0, A_0)), IF1Ta(NAND(region_MEST, car_value_g), coeficientPos(O2, -0.7), sub(coeficientPos(add(Null, O2), addf(-0.2, -0.7)), Null)), IF0Ta(IF1Tb(A_1, E_0, A_0), IF0Ta(D_1, add(add(O1, O0), divPos(O1, 0.4)), Null), add(IF1Ta(A_1, O0, O1), O2))), O1), IF1Ta(A_1, divPos(O1, -0.7), O1))"

            },
            "B" : 
            { 91.53 : "IF0Ta(B_1, coeficientPos(IF1Ta(NOT(IF1Tb(IF1Tb(car_value_e, car_value_a, group_size_2), time_21_24, AND(time_15_18, car_value_i))), sub(IF1Ta(NOT(B_1), divPos(O1, -0.7), divPos(IF1Ta(D_3, O0, O1), -0.8)), O0), sub(IF1Ta(IF0Tb(E_0, car_age_10_15, C_previous_2), sub(sub(Null, O1), Null), Null), sub(O0, add(O1, O0)))), subf(subf(subf(IF0Tf(day_thursday, 0.4, -0.7), multf(0.0, 0.0)), subf(divf(-0.7, -0.9), subf(-0.8, multf(addf(0.2, 0.4), addf(0.6, 0.4))))), multf(divf(addf(0.2, IF1Tf(age_oldest_20_24, 0.9, -0.6)), subf(-0.4, 0.1)), subf(multf(0.4, -1.0), addf(multf(addf(0.6, 0.4), -0.7), IF1Tf(day_tuesday, -0.2, 0.6)))))), O0)"

            },
            "C" : 
            {


            },
            "D" : 
            {  93.03 : "add(divPos(divPos(O0, -0.9), IF1Tf(D_1, subf(divf(-0.8, -0.3), divf(0.7, 0.3)), -0.2)), add(IF1Ta(D_2, add(IF1Ta(NOT(time_6_9), divPos(Null, 0.7), IF1Ta(NAND(time_15_18, car_age_10_15), sub(Null, Null), coeficientPos(O0, -0.8))), add(Null, O2)), O1), coeficientPos(IF1Ta(D_1, O2, O1), 0.1)))"

            },
            "E" : 
            {  91.148 : "coeficientPos(O0, IF1Tf(E_1, IF1Tf(E_1, 0.5, IF1Tf(E_1, 0.5, -0.4)), -0.4))"

            },
            "F" : 
            {

            },
            "G" : 
            {

            }

            
        }

    def get(self, param):
        return self.individuals[param]



