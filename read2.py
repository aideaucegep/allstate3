#!/usr/bin/python
# This package preload the csv file, and produce a list of lists, every item of 
# the main list represent a full shopping and buying experiments

import csv
import random
class read2(object):

    def __init__(self, length=0, name = "train.csv"):
        print "Reading..."
        self.currentIndex = 0
        self.data, self.length, self.header = self.readData(length, name)
        print "Ready!, (", self.length, "purchases )"


    def readData(self, length, name):
        data = []
        with open(name, 'rb') as f:
            r = csv.reader(f)
            header = r.next()
            purchaseIndex = header.index("record_type")
            customer_IDIndex = header.index("customer_ID")
            print customer_IDIndex
            data = [[]]
            i = 0
            for row in r:
                customer_ID = row[customer_IDIndex]
                row = [bool(int(j)) for j in row]
                row[customer_IDIndex] = int(customer_ID)
                data[-1].append(row)
                #print data[-1][-1]
                if row[purchaseIndex] == 1: #is a purchase
                    data.append([])

                    i += 1
                if i > length and length > 1:
                    break
            f.close
            data.pop() #remove empty last result
        return data, len(data), header


    def __iter__(self):
        return (self)

    def __len__(self):
        return self.length


    def head(self):
        return self.header

    def name(self, index):
        return self.header[index]

    def index(self, name):
        return self.header.index(name)

    def next(self): #return current observation, change reading head to next line
        if self.currentIndex < self.length:
            self.currentIndex += 1
            return self.data[self.currentIndex-1]
        else:
            raise StopIteration

    def reset(self):
        self.currentIndex = 0

    def current(self, index):
        if index <= self.length:
            return self.data[index]
        else:
            return False

    def last(self):
        return self.data[-1]

    def randNext(self, prob):
        # iterate over the set, with a probability of 
        # stopping and returning current set
        while True:
            self.currentIndex +=1
            rnd = random.random()

            if self.currentIndex > self.length:
                break
            #print self.currentIndex, "of", self.length , rnd, prob
            if rnd < prob:
                #print "!"
                break
                

        return self.current(self.currentIndex)

    def nRandNext(self, n):
        # try to return n randomly chosen experiemnts
        # from the data
        ls = []
        ls = random.sample(self.data, n)
        return ls

