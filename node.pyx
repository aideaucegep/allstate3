#!/usr/bin/python

import cython
from cpython cimport bool
import operator
import math
import random
from types import *

import numpy
cimport numpy
from deap import gp


def AND(bool u,bool v):
    return (u and v)
    
def NOT(bool u):
    return not u
    
def OR(bool u, bool v):
    return (u or v)
    
def XOR(bool u, bool v):
    return operator.xor(u,v)

def NAND(bool u, bool v):
    return NOT(AND(u,v))

def NOR(bool u, bool v):
    return NOT(OR(u,v))

def IF1Tf(bool u, float a, float b):
    #print (u)
    if(u):
        return a
    else:
        return b
        
def IF0Tf(bool u, float a, float b):
    #we just inverse returm value
    return IF1Tf((u), b, a)

def IF1Tb(bool u, bool a, bool b):
    #print (u)
    if(u):
        return a
    else:
        return b
        
def IF0Tb(bool u, bool a, bool b):
    #we just inverse returm value
    return IF1Tb((u), b, a)

def IF1Ta(bool u, numpy.ndarray a, numpy.ndarray b):
    #print (u)
    if(u):
        return a
    else:
        return b
        
def IF0Ta(bool u, numpy.ndarray a, numpy.ndarray b):
    #we just inverse returm value
    return IF1Ta((u), b, a)


def add( numpy.ndarray u, numpy.ndarray v):
    return u+v

def sub( numpy.ndarray u, numpy.ndarray v):
    return u-v

def addf( float u, float v):
    return u+v

def subf( float u, float v):
    return u-v

def multf( float u, float v):
    return u*v

def divf( float u, float v):
    if(v != 0):
        return u/v
    else:
        return u

def coeficientPos(numpy.ndarray u, float f):
    return f*u

def coeficientNeg(numpy.ndarray u, float f):
    return (-1)*f*u

def divPos(numpy.ndarray u, float f):
    if(f != 0):
        return (1/f)*u
    else:
        return u

def initNode(n, predictionArity):
    pset = gp.PrimitiveSetTyped("MAIN", [bool]*n, numpy.ndarray)
    
    #pset.addPrimitive(identityo, [object], object)
    #pset.addPrimitive(identityf, [float], float)
    #pset.addPrimitive(identityb, [bool], bool)
    #pset.addPrimitive(identitya, [numpy.ndarray], numpy.ndarray)

    pset.addPrimitive(AND, [bool, bool], bool)
    pset.addPrimitive(NOT, [bool], bool)
    pset.addPrimitive(OR, [bool, bool], bool)
    pset.addPrimitive(XOR, [bool, bool], bool)
    pset.addPrimitive(NAND, [bool, bool], bool)
    pset.addPrimitive(NOR, [bool, bool], bool)

    pset.addPrimitive(coeficientPos, [numpy.ndarray, float], numpy.ndarray)
    #pset.addPrimitive(coeficientNeg, [numpy.ndarray, float], numpy.ndarray)

    pset.addPrimitive(divPos, [numpy.ndarray, float], numpy.ndarray)
    
    #pset.addPrimitive(IF1, [bool, numpy.ndarray], numpy.ndarray)
    #pset.addPrimitive(IF0, [bool, numpy.ndarray], numpy.ndarray)

    
    pset.addPrimitive(IF1Tf, [bool, float, float], float)
    pset.addPrimitive(IF0Tf, [bool, float, float], float)
    pset.addPrimitive(IF1Tb, [bool, bool, bool], bool)
    pset.addPrimitive(IF0Tb, [bool, bool, bool], bool)
    pset.addPrimitive(IF1Ta, [bool, numpy.ndarray, numpy.ndarray], numpy.ndarray)
    pset.addPrimitive(IF0Ta, [bool, numpy.ndarray, numpy.ndarray], numpy.ndarray)

    pset.addPrimitive(add, [numpy.ndarray, numpy.ndarray], numpy.ndarray)
    pset.addPrimitive(sub, [numpy.ndarray, numpy.ndarray], numpy.ndarray)

    pset.addPrimitive(addf, [float, float], float)
    pset.addPrimitive(subf, [float, float], float)
    pset.addPrimitive(multf, [float, float], float)
    pset.addPrimitive(divf, [float, float], float)

    # no use for true and false, they just change from agate to another and change conditional to identity
    #pset.addTerminal(True, bool, "TRUE")
    #pset.addTerminal(False, bool, "FALSE")    
    
    if(predictionArity == 2):
        pset.addTerminal(numpy.array([1,0]), numpy.ndarray, "O0")
        pset.addTerminal(numpy.array([0,1]), numpy.ndarray, "O1")
        pset.addTerminal(numpy.array([0,0]), numpy.ndarray, "Null")
    elif(predictionArity == 3):
        pset.addTerminal(numpy.array([1,0,0]), numpy.ndarray, "O0")
        pset.addTerminal(numpy.array([0,1,0]), numpy.ndarray, "O1")
        pset.addTerminal(numpy.array([0,0,1]), numpy.ndarray, "O2")
        pset.addTerminal(numpy.array([0,0,0]), numpy.ndarray, "Null")
    elif(predictionArity == 4):
        pset.addTerminal(numpy.array([1,0,0,0]), numpy.ndarray, "O0")
        pset.addTerminal(numpy.array([0,1,0,0]), numpy.ndarray, "O1")
        pset.addTerminal(numpy.array([0,0,1,0]), numpy.ndarray, "O2")
        pset.addTerminal(numpy.array([0,0,0,1]), numpy.ndarray, "O3")
        pset.addTerminal(numpy.array([0,0,0,0]), numpy.ndarray, "Null")

    for i in xrange(-10, 10):
        pset.addTerminal(i/(10.0), float, str(i/10.0))


    # Letter specific terminaL
    # A
    #pset.addPrimitive(SOL1, [bool, bool], numpy.ndarray)
    #pset.addPrimitive(SOL2, [bool], numpy.ndarray)

    pset.renameArguments(ARG0="record_type")
    pset.renameArguments(ARG1="day_sunday")
    pset.renameArguments(ARG2="day_monday")
    pset.renameArguments(ARG3="day_tuesday")
    pset.renameArguments(ARG4="day_wednesday")
    pset.renameArguments(ARG5="day_thursday")
    pset.renameArguments(ARG6="day_friday")
    pset.renameArguments(ARG7="day_saturday")
    pset.renameArguments(ARG8="time_0_3")
    pset.renameArguments(ARG9="time_3_6")
    pset.renameArguments(ARG10="time_6_9")
    pset.renameArguments(ARG11="time_9_12")
    pset.renameArguments(ARG12="time_12_15")
    pset.renameArguments(ARG13="time_15_18")
    pset.renameArguments(ARG14="time_18_21")
    pset.renameArguments(ARG15="time_21_24")

    pset.renameArguments(ARG16="region_NENG")
    pset.renameArguments(ARG17="region_MEST")
    pset.renameArguments(ARG18="region_GLAK")
    pset.renameArguments(ARG19="region_PLNS")
    pset.renameArguments(ARG20="region_SEST")
    pset.renameArguments(ARG21="region_SWST")
    pset.renameArguments(ARG22="region_RKMT")
    pset.renameArguments(ARG23="region_FWST")

    pset.renameArguments(ARG24="group_size_1")
    pset.renameArguments(ARG25="group_size_2")
    pset.renameArguments(ARG26="group_size_3")
    pset.renameArguments(ARG27="group_size_4")
    pset.renameArguments(ARG28="homeowner")
    pset.renameArguments(ARG29="car_age_0_5")
    pset.renameArguments(ARG30="car_age_5_10")
    pset.renameArguments(ARG31="car_age_10_15")
    pset.renameArguments(ARG32="car_age_15P")
    pset.renameArguments(ARG33="car_value_a")
    pset.renameArguments(ARG34="car_value_b")
    pset.renameArguments(ARG35="car_value_c")
    pset.renameArguments(ARG36="car_value_d")
    pset.renameArguments(ARG37="car_value_e")
    pset.renameArguments(ARG38="car_value_f")
    pset.renameArguments(ARG39="car_value_g")
    pset.renameArguments(ARG40="car_value_h")
    pset.renameArguments(ARG41="car_value_i")
    pset.renameArguments(ARG42="car_value_NA")
    pset.renameArguments(ARG43="risk_factor_1")
    pset.renameArguments(ARG44="risk_factor_2")
    pset.renameArguments(ARG45="risk_factor_3")
    pset.renameArguments(ARG46="risk_factor_4")
    pset.renameArguments(ARG47="risk_factor_NA")
    pset.renameArguments(ARG48="age_oldest_16_19")
    pset.renameArguments(ARG49="age_oldest_20_24")
    pset.renameArguments(ARG50="age_oldest_25_40")
    pset.renameArguments(ARG51="age_oldest_40_55")
    pset.renameArguments(ARG52="age_oldest_55P")
    pset.renameArguments(ARG53="age_youngest_16_19")
    pset.renameArguments(ARG54="age_youngest_20_24")
    pset.renameArguments(ARG55="age_youngest_25_40")
    pset.renameArguments(ARG56="age_youngest_40_55")
    pset.renameArguments(ARG57="age_youngest_55P")
    pset.renameArguments(ARG58="married_couple")
    pset.renameArguments(ARG59="C_previous_1")
    pset.renameArguments(ARG60="C_previous_2")
    pset.renameArguments(ARG61="C_previous_3")
    pset.renameArguments(ARG62="C_previous_4")
    pset.renameArguments(ARG63="C_previous_NA")
    pset.renameArguments(ARG64="duration_previous_0_2")
    pset.renameArguments(ARG65="duration_previous_2_5")
    pset.renameArguments(ARG66="duration_previous_5_9")
    pset.renameArguments(ARG67="duration_previous_9P")
    pset.renameArguments(ARG68="duration_previous_NA")
    pset.renameArguments(ARG69="A_0")
    pset.renameArguments(ARG70="A_1")
    pset.renameArguments(ARG71="A_2")
    pset.renameArguments(ARG72="B_0")
    pset.renameArguments(ARG73="B_1")
    pset.renameArguments(ARG74="C_1")
    pset.renameArguments(ARG75="C_2")
    pset.renameArguments(ARG76="C_3")
    pset.renameArguments(ARG77="C_4")
    pset.renameArguments(ARG78="D_1")
    pset.renameArguments(ARG79="D_2")
    pset.renameArguments(ARG80="D_3")
    pset.renameArguments(ARG81="E_0")
    pset.renameArguments(ARG82="E_1")
    pset.renameArguments(ARG83="F_0")
    pset.renameArguments(ARG84="F_1")
    pset.renameArguments(ARG85="F_2")
    pset.renameArguments(ARG86="F_3")
    pset.renameArguments(ARG87="G_1")
    pset.renameArguments(ARG88="G_2")
    pset.renameArguments(ARG89="G_3")
    pset.renameArguments(ARG90="G_4")
    pset.renameArguments(ARG91="cost_0_605")
    pset.renameArguments(ARG92="cost_605_635")
    pset.renameArguments(ARG93="cost_635_665")
    pset.renameArguments(ARG94="cost_665P")
    
    return pset

##########################################
# old function
def identity(u):
    return u

def identityf(u):
    return u

def identityo(u):
    return u

def identitya(u):
    return u

# head.append("record_type")
# head.extend(["day:sunday", "day:monday", "day:tuesday", "day:wednesday", "day:thursday", "day:friday", "day:saturday"])
# head.extend(["time:0-3", "time:3-6", "time:6-9", "time:9-12", "time:12-15", "time:15-18", "time:18-21", "time:21-24"])
# head.extend(["state:AL", "state:AR", "state:CO", "state:CT", "state:DC", "state:DE", "state:FL", "state:GA", "state:IA", "state:ID", "state:IN", "state:KS", "state:KY", "state:MD", "state:ME", "state:MO", "state:MS", "state:MT", "state:ND", "state:NE", "state:NH", "state:NM", "state:NV", "state:NY", "state:OH", "state:OK", "state:OR", "state:PA", "state:RI", "state:SD", "state:TN", "state:UT", "state:WA", "state:WI", "state:WV", "state:WY"])
# head.extend(["group_size:1", "group_size:2", "group_size:3", "group_size:4"])
# head.append("homeowner")
# head.extend(["car_age:0-5", "car_age:5-10", "car_age:10-15", "car_age:15+"])
# head.extend(["car_value:a", "car_value:b", "car_value:c", "car_value:d", "car_value:e", "car_value:f", "car_value:g", "car_value:h", "car_value:i", "car:value:NA"])
# head.extend(["risk_factor:1", "risk_factor:2", "risk_factor:3", "risk_factor:4", "risk_factor:NA"])
# head.extend(["age_oldest:16-19", "age_oldest:20-24", "age_oldest:25-40", "age_oldest:40-55", "age_oldest:55+"])
# head.extend(["age_youngest:16-19", "age_youngest:20-24", "age_youngest:25-40", "age_youngest:40-55", "age_youngest:55+"])
# head.append("married_couple")
# head.extend(["C_previous:1", "C_previous:2", "C_previous:3", "C_previous:4", "C_previous:NA #!has_previous"])
# head.extend(["duration_previous:0-2", "duration_previous:2-5", "duration_previous:5-9", "duration_previous:9+", "duration_previous:NA"])
# head.extend(["A:0", "A:1", "A:2"])
# head.extend(["B:0", "B:1"])
# head.extend(["C:1", "C:2", "C:3", "C:4"])
# head.extend(["D:1", "D:2", "D:3"])
# head.extend(["E:0", "E:1"])
# head.extend(["F:0", "F:1", "F:2", "F:3"])
# head.extend(["G:1", "G:2", "G:3", "G:4"])
# head.extend(["cost:0-605", "cost:605-635", "cost:635-665", "cost:665+"])
