#!/bin/sh
sudo apt-get install htop
sudo apt-get install python-dev
sudo apt-get install python-pip
sudo apt-get install expect-dev
sudo pip install scoop
sudo pip install numpy
sudo pip install cython
sudo apt-get install mercurial
sudo pip install -e hg+https://code.google.com/p/deap/@default#egg=Package
wget https://www.dropbox.com/s/k6s5zgo950xoabn/cleandata.csv
wget https://www.dropbox.com/s/9bqnfnn52n9f38v/cleandataBEA.csv
python setup.py build_ext --inplace

