#!/usr/bin/python
# cython: profile=true
__author__ = 'PyWebDesign'
import sys, getopt
from scoop import futures
import operator
import math
import random
import numpy

from deap import base
from deap import creator
from deap import tools
from deap import gp

import algorithm
import read2
import node

 ###############################################################
 #  Option_name | Possible values   |    n      |    offset    #
 #      A       |       0,1,2       |    3      |       0      #
 #      B       |-      0,1         |-   2      |       0      #
 #      C       |       1,2,3,4     |    4      |       1      #
 #      D       |-      1,2,3       |-   3      |       1      #
 #      E       |       0,1         |    2      |       0      #
 #      F       |       0,1,2,3     |    4      |       0      #
 #      G       |       1,2,3,4     |    4      |       1      #
 ###############################################################
predictionParameters = {
    "A" : { "length" : 3, "offset" : 0 },
    "B" : { "length" : 2, "offset" : 0 },
    "C" : { "length" : 4, "offset" : 1 },
    "D" : { "length" : 3, "offset" : 1 },
    "E" : { "length" : 2, "offset" : 0 },
    "F" : { "length" : 4, "offset" : 0 },
    "G" : { "length" : 4, "offset" : 1 }
}
# Read command line args
myopts, args = getopt.getopt(sys.argv[1:],"p:n:i:g:s")
 
###############################
# o == option
# a == argument passed to the o
###############################

###############################
# -pp   = Prediction Parameter  | Parameter to preditc, should be one of: A, B, C, D, E, F, G
# -n    = nExp                  | Number of purchase experiments to be tested for each individual
# -ni   = nIndiv                | Approximate number of different individuals on each generation, exactly n for the first generations
# -ng   = nGen                  | Number of generation to evolve in
###############################

#default value:
predictionParameter = "A"
nExps = 60
nRead = 0
nIndiv = 30
nGen = 10
dataFileName = "cleandataBEAordered.csv"

if len(myopts) < 1:
    print("Usage: -p Parameter to predict -n nExp -i nIndiv -g nGen")
    exit()

for o, a in myopts:
    print o, a
    if o == '-p':
        predictionParameter = a
    elif o == '-n':
        nExps = int(a)
    elif o == '-i':
        nIndiv = int(a)
    elif o == '-g':
        nGen = int(a)
    elif o == '-s':
        nRead = 10000

# TODO refactor, this is not usefull to copy around data for nothing
param = predictionParameters[predictionParameter]
predictionParameterLength = param['length']
predictionOffset = param['offset']

#Hall of fame, number of best individual to show
nHof = 20 #30

#Min and Max height of new generate random individual
genMinTreeHeight = 1
genMaxTreeHeight = 6

#number of element describing an event in the data set
n=95

#TODO make it load the right dataset everytime

read = read2.read2(nRead, dataFileName)
record_type_index = read.index("record_type")

index = read.index(str(predictionParameter) + ":" + str(predictionOffset))

print "position:",
for count in range(predictionParameterLength):
    print index + count,
print ""
#set up the primitive set, see node.py
pset = node.initNode(n, predictionParameterLength)

creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
creator.create("Individual", gp.PrimitiveTree, fitness=creator.FitnessMin)

toolbox = base.Toolbox()

# scoop multi core line, comment for single core!
toolbox.register("map", futures.map)

toolbox.register("expr", gp.genHalfAndHalf, pset=pset, min_=genMinTreeHeight, max_=genMaxTreeHeight)
toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.expr)
toolbox.register("population", tools.initRepeat, list, toolbox.individual)
toolbox.register("compile", gp.compile, pset=pset)


def evalSymbReg(individual, gen):
    # gen : current gen number
    # Transform the tree expression in a callable function
    func = toolbox.compile(expr=individual)
    score = 0
    ls = numpy.array([0.0]*predictionParameterLength)
    nExperiments = int( 10 + (2.72**(-3.0*((gen-nGen)**2.0)/(nGen**2.0)))*nExps)
    for experiment in read.nRandNext(nExperiments):
        #number of full experiment containing a sale
        for item in experiment:
            if item[record_type_index] != 1: #si c'est un magasinage
                a = func(*item[0:n])
                ls += a
                #try:
                #    a = func(*item[0:n])
                #    ls += a
                #    #a = func(*map(bool, map(int, item[0:n])))
                #except :
                #    score += 10 #mauvais, contient une erreur
            else: #sinon un achat qu'il faut prdire par les magasinage
                #it seamns having the script default to A0 make it a lot better at predicting, when all a = nothing, ls is an empty array and
                #is is evaluated as A0; this is good somehow but may not be that good otherwise, like for b,c,d,e,f,g for example
                #it should be set to the most probable here, we have to take note to add this when productin the results
                m = min(ls)
                #if(m == 0): #catch empty prediction
                #    score += 10

                result = [i for i, j in enumerate(ls) if j == m]
                #print ls, m, result[0]
                if(int(item[index + result[0]]) == 1):
                    score -= 1
                    #score += len(individual) / 500000.0

                ls.fill(0)
    return score / float(nExperiments),

toolbox.register("evaluate", evalSymbReg)
toolbox.register("select", tools.selTournament, tournsize=3)
toolbox.register("mate", gp.cxOnePoint)
toolbox.register("expr_mut", gp.genFull, min_=0, max_=2)
toolbox.register("mutate", gp.mutUniform, expr=toolbox.expr_mut, pset=pset)

def initPop(n):
    pop = []
    # add individual to the individual list here

    for i in xrange(n):
        pop.append(toolbox.individual())
    return pop



def main(argv):
    #pop = toolbox.population(n=nIndiv)
    pop = initPop(nIndiv)
    hof = tools.HallOfFame(nHof)

    #print pop
    stats_fit = tools.Statistics(lambda ind: ind.fitness.values)
    stats_size = tools.Statistics(len)
    mstats = tools.MultiStatistics(fitness=stats_fit, size=stats_size)
    mstats.register("avg", numpy.mean)
    mstats.register("std", numpy.std)
    mstats.register("min", numpy.min)
    mstats.register("max", numpy.max)

    print "predictionParameter", predictionParameter
    print "predictionParameterLength", predictionParameterLength
    print "nExps", nExps
    print "nRead", nRead
    print "nIndiv", nIndiv
    print "nHof", nHof
    print "nGen", nGen
    print "genMinTreeHeight", genMinTreeHeight
    print "genMaxTreeHeight", genMaxTreeHeight
    print "Data", dataFileName

    pop, log = algorithm.eaSimple(pop, toolbox, 0.4, 0.3, nGen, stats=mstats,
                                   halloffame=hof, verbose=True)
    print log
    i = 0
    for ind in hof:
        print i, ind.fitness.values, ind 
        i+=1

    return pop, log, hof

if __name__ == "__main__":
    main(sys.argv[1:])
